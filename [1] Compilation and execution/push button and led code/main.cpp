/*
 * You need to both compile and build this project in order to work.
 * You need wiringPi library for this code. this libray only works with raspberry
 * make sure you have gpio, type 'gpio -v' on the command line.
 * Make sure you have edited the build/set build commands to both compile and build to include the  -lwiringPi at the end.
 */

#include <iostream>
#include <wiringPi.h>

using namespace std;

int pin_val=1; //store the value of the pin

int main()
{
	wiringPiSetup(); //what this does?
	pinMode(0, OUTPUT);//set pin to output
	pinMode(1, INPUT);//set pin to input
	
	while(1) //my loop
	{
		pin_val = digitalRead(1);//read the pin value (0 or 1)
		if(pin_val==0)//if button is pressed
		{
			digitalWrite(0, HIGH); //write 1 to pin 0
			delay(200); //help with button debounce (not neccesary but good to have around)
		}
		else
		{
			digitalWrite(0,LOW); //else write 0 to pin 0
		}
	}
	
	return 0;
}
 
