#!/usr/bin/python
from tkinter import *                   #import the tkinker lib
root = Tk()                     #create the root object
root.wm_title("BioSensor")              #Sets the title of the window
root.configure(bg="black")              #change the background color to black
#root.geometry("1000x900")              #Specify the window dimension
root.attributes("-fullscreen",True)         #set to fullscreen

class Button:
	
	def __init__(self, padx, pady):
		self.padx = padx
		self.pady = pady

Temperature = StringVar()                                                   #store the temperature

#Disable the fullscreen
def endProgram(event):
    root.attributes("-fullscreen", False);

#Update the label of the labelTemperature
#from the entryTemperature 
def displayTemp():
    labelTemperature["text"] = Temperature.get()    

 
#Create labels
labelNanoplasmas= Label(root, text = "Nanoplasmas Project") 
labelTemperature = Label(root, text = "Temperature Set")    
labelTime = Label(root, text = "Time Set")

#Create Entries
entryTemperature = Entry( root , textvariable = Temperature )               #Save the temperature       
entryTime = Entry( root )

#Create buttons
ButtonStart = Button ( root , text = " Start " , command = displayTemp )    #run the command when clicked

#Set the position of the label
labelTemperature.grid(row=0, column=0)              
labelTime.grid ( row = 1 , column = 0 )
labelNanoplasmas.grid(row = 3 , column = 3 )

#Set the positionn of entries
entryTemperature.grid( row = 0 , column = 1 )           
entryTime.grid (row = 1 , column = 1)

#Set the position of Buttons
ButtonStart.place(x=100, y=100, in_=root) #( row = 2 , column = 1 )

    
root.bind("<Escape>", endProgram)               #Call endProgram when esc is pressed

root.mainloop()                                 #Starts the gui loop















